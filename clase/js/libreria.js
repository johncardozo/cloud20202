const sumar = (num1, num2) => {
  return num1 + num2;
};

const restar = (num1, num2) => {
  return num1 - num2;
};

const DIAS_SEMANA = 7

// Exportar a otros archivos
export { sumar, restar, DIAS_SEMANA };