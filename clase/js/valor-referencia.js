function agregar(lista, elemento) {
  lista.push(elemento);
  elemento += 1;
}

let elementos = [1, 2, 3, 4, 5];
let num = 10;

console.log(elementos);
console.log(num);
// arreglos por referencia
agregar(elementos, num);

console.log(elementos);
console.log(num);

// ARROW FUNCTIONS
function mensaje_clasico(){
    console.log('clasico')
}

const mensaje_arrow = () => console.log('arrow')

mensaje_clasico()
mensaje_arrow()

const suma = (num1, num2) => {
    let resultado;
    resultado = num1 + num2;
    return resultado;
}
