var firstName = "John";
var lastName = "Cardozo";

console.log(firstName, lastName);
/*
\'
\"
\n
\t
*/
var phrase1 = "este es el\tcaracter ";
console.log(phrase1);

var phrase2 = `caracter=' y caracter="`; 
console.log(phrase2);
// concatenate
var theMovie = "Star " + "Wars";
console.log(theMovie);
// Operator += concatenate
theMovie += ": The Return of the Jedi";
console.log(theMovie);
// Concatenate different types
var year = 1983;
theMovie += " - " + year;
console.log(theMovie); 
// Length of a string
var length = theMovie.length;
console.log(length);
// Position in string (zero based)
var letter = theMovie[0];
console.log(letter);
theMovie[0] = "H"; // it doesn't raise en error
console.log(theMovie);

