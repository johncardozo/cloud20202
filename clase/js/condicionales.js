let x = 6;
// UNICA OPCION
if (x != 5) {
  console.log("la variable tiene valor diferente 5");
}
// 2 OPCIONES
if (x % 2 == 0) {
  console.log("El numero es par");
} else {
  console.log("El numero es impar");
}
// MULTIPLES OPCIONES
let valor = -9;
if (valor == 0) {
  console.log("es cero");
} else if (valor > 0) {
  console.log("es positivo");
} else {
  // opcional
  console.log("Es negativo");
}

// SWITCH
let dia = 10;
switch (dia) {
  case 1:
    console.log("lunes");
    break;
  case 2:
    console.log("martes");
    break;
  case 3:
    console.log("miercoles");
    break;
  case 4:
    console.log("jueves");
    break;
  case 5:
    console.log("viernes");
    break;
  case 6:
  case 7:
    console.log("FIN DE SEMANA");
    break;

  default:
    console.log("El dia no es válido");
    break;
}

// == vs ===
let numero = 3;
let cadena = "3";
if (numero === cadena) {
  console.log("SON IGUALES");
} else {
  console.log("SON DIFERENTES");
}

// TERNARIAS
let plural = false;
console.log("gato" + (plural ? "s" : ""));
