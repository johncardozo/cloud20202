document.addEventListener("DOMContentLoaded", function () {
  // console.log("INICIO");

  let titulo = document.getElementById("titulo");
  // console.log(titulo);

  let lis = document.getElementsByTagName("li");
  // console.log(lis);

  let elementos = document.getElementsByClassName("encabezado");
  // console.log(elementos);

  let verdes = document.querySelectorAll(".continentes li.norte");
  // console.log(verdes);

  let titulo2 = document.querySelector("#titulo");
  //console.log(titulo2);
  titulo.innerHTML = "ESTE ES MI NUEVO TITULO";

  let linkUSTA = document.getElementById("linkUSTA");
  linkUSTA.href = "http://www.mit.edu";
  linkUSTA.style.color = "violet";
  linkUSTA.style.textDecoration = "none";

  let imagen = document.getElementById("imagen");
  let altImagen = imagen.getAttribute("alt");
  // console.log(altImagen);
  imagen.setAttribute("src", "img/moon.png");

  let est1 = document.getElementById("est1");
  est1.classList.add("estudiante");

  let est2 = document.getElementById("est2");
  est2.classList.remove("estudiante");

  function cambiarTexto(elemento) {
    elemento.innerHTML = "Cambio el texto desde una funcion";
  }

  function cambiarTextoH3() {
    document.getElementById("titulo3").innerHTML =
      "Cambio el texto H3 desde una funcion";
  }

  let titulo3 = document.getElementById("titulo3");
  titulo3.onclick = cambiarTextoH3;

  let titulo4 = document.getElementById("titulo4");

  const handlerTitulo40 = () => {
    // Obtiene el titulo 3
    let t3 = document.getElementById("titulo3");
    t3.innerHTML = "Cambio el texto";
  };
  function handlerTitulo41() {
    // Obtiene el titulo 2
    let t2 = document.getElementById("titulo2");
    t2.classList.add("estudiante");
  }

  titulo4.addEventListener("click", handlerTitulo40);
  titulo4.addEventListener("click", handlerTitulo41);

  let boton = document.getElementById("boton");
  boton.addEventListener("click", () => {
    console.log("click");
  });
  boton.addEventListener("mouseover", () => {
    console.log("mouseover");
  });
  boton.addEventListener("mouseout", () => {
    console.log("mouseout");
  });

  let area = document.getElementById("area");
  // console.log(area);
  // console.log(area.children); // Acceso a los hijos
  let primerHijo = area.firstElementChild;
  let ultimoHijo = area.lastElementChild;
  //console.log(area.children[0]);
  // console.log(primerHijo);
  // console.log(ultimoHijo);

  // console.log(primerHijo.parentElement); // Padre
  // console.log(primerHijo.nextElementSibling); // Siguiente hermano
  // console.log(ultimoHijo.previousElementSibling); // Hermano anterior

  // let ld = document.getElementById("departamentos");

  // for (let i = 0; i < ld.children.length; i++) {
  //   console.log(ld.children[i]);
  // }

  // Array.from(ld.children).forEach(elemento => {
  //   console.log(elemento);
  // });

  let divEquipos = document.getElementById("equipos");
  // Crea la lista <ul>
  let ul = document.createElement("ul");
  ul.setAttribute("id", "ulEquipos");
  // Crea los elementos hijos <li>
  let li1 = document.createElement("li");
  li1.innerHTML = "Arsenal";
  let li2 = document.createElement("li");
  li2.innerHTML = "Manchester United";
  let li3 = document.createElement("li");
  li3.innerHTML = "Totenham";
  // Agrega los <li> a la lista <ul>
  ul.appendChild(li1);
  ul.appendChild(li2);
  ul.appendChild(li3);
  // Agregar la lista <ul> al div equipos
  divEquipos.appendChild(ul);

  let miLista = divEquipos.firstElementChild;
  let le = Array.from(miLista.children);
  le.forEach((e) => {
    e.classList.add("equipo");
  });

  // Click en el boton de eliminar
  document.getElementById("botonE").addEventListener("click", () => {
    // Elimina el ultimo hijo de la lista
    document.getElementById("ulEquipos").lastElementChild.remove();
  });
  
});
