document.addEventListener("DOMContentLoaded", () => {
  // Actualizar el contador
  const actualizarContador = () => {
    // Obtiene la lista
    let ul = document.getElementById("lista");
    // Obtiene el contador
    let contador = document.getElementById("contador");
    // Actualiza el contador
    contador.innerHTML = ul.children.length;
  };

  // Click en el checkbox
  const clickCheckbox = (evento) => {
    // Obtiene el elemento al cual se le hizo click
    let elemento = evento.target;
    // Obtiene el <li> padre
    let padre = elemento.parentElement;
    // Cambia la clase css del padre <li>
    padre.classList.toggle("terminada");
  };

  // Click en el botón eliminar tarea
  const clickEliminar = (evento) => {
    // Obtiene el boton al cual se le hizo click
    let boton = evento.target;
    // Obtiene el <li> padre
    let padre = boton.parentElement;
    // Eliminar el elemento
    padre.remove();
    // Actualiza el contador
    actualizarContador();
  };

  // Obtiene el boton para agregar tarea
  let boton = document.getElementById("boton");

  // Click en el botón Agregar Tarea
  boton.addEventListener("click", () => {
    // Obtiene el input
    let input = document.getElementById("tarea");
    // Obtiene el texto digitado
    let texto = input.value;
    // Crea el elemento <li>
    let li = document.createElement("li");
    // Crea el elemento <input:checkbox>
    let checkbox = document.createElement("input");
    checkbox.setAttribute("type", "checkbox");
    checkbox.addEventListener("click", clickCheckbox);
    // Crea el elemento de tipo texto
    let elementoTexto = document.createTextNode(texto);
    // Crea el boton para eliminar la tarea
    let botonEliminar = document.createElement("button");
    botonEliminar.innerText = "x";
    botonEliminar.addEventListener("click", clickEliminar);
    // Agrega los elementos al <li>
    li.appendChild(checkbox);
    li.appendChild(elementoTexto);
    li.appendChild(botonEliminar);
    // Obtiene la lista <ul>
    let ul = document.getElementById("lista");
    // Agrega el <li> al <ul>
    ul.appendChild(li);
    // Actualiza el contador
    actualizarContador();
    // Limpia el input
    input.value = "";
  });
});
